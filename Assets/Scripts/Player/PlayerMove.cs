﻿using System;
using UnityEngine;

namespace Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerMove : MonoBehaviour
    {
        [SerializeField] 
        private PlayerInput _input;
        
        [SerializeField] 
        private float _speed = 5f;

        [SerializeField, Range(0, 1f)] 
        private float _speedSmoothTime = .1f;
        
        private Rigidbody2D _rigidbody;
        private float _velocity;
        
        private void Awake() => 
            _rigidbody = GetComponent<Rigidbody2D>();

        private void FixedUpdate() => 
            _rigidbody.MovePosition(_rigidbody.position + Movement());

        private Vector2 Movement()
        {
            var targetVelocityX = _input.Direction.x * _speed;
            _velocity = Mathf.SmoothDamp(_velocity, targetVelocityX, ref _velocity, _speedSmoothTime);
            var movement = new Vector2(_velocity, Physics.gravity.y);
            return movement * Time.fixedDeltaTime;
        }
    }
}